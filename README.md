## Документация к программе для суммирования характеристик Платоновых тел.

## Автор
**Горбаренко Вадим**

## Описание программы.
Программа для суммирования характеристик Платоновых тел: углов, ребер, сторон

## Решение задачи
Для решения задачи, используется функция суммирования

## Модули программы
- vpd3.py

## Используемые сторонние модули и библиотеки
* [ ] sys
* [ ] unittest

## Код программы
    import sys
    import unittest
    
    
    class NamesTestCase(unittest.TestCase):
        def test_test(self):
            """
            Тест функции проверки ввода.
    
            Рассматривается случай: на вход подается 'Oct Dod Ico'.
            >>test(edge, 'Oct Dod Ico')
            ['Oct', 'Dod', 'Ico']
            """
            edge = {"Tetrahedron": 4, "Hexahedron": 6, "Octahedron": 8, "Dodecahedron": 12, "Icosahedron": 20,
                    'Tet': 4, 'Hex': 6, "Oct": 8, "Dod": 12,
                    "Ico": 20}
            test_ = test(edge, 'Oct Dod Ico')
            self.assertEqual(test_, ['Oct', 'Dod', 'Ico'])
    
        def test_test2(self):
            """
            Тест функции проверки ввода.
    
            Рассматривается случай: на вход подается ''.
            >>test(edge, '')
            []
            """
            edge = {"Tetrahedron": 4, "Hexahedron": 6, "Octahedron": 8, "Dodecahedron": 12, "Icosahedron": 20,
                    'Tet': 4, 'Hex': 6, "Oct": 8, "Dod": 12,
                    "Ico": 20}
            test_ = test(edge, '')
            self.assertEqual(test_, [])
    
        def test_work_edge(self):
            """
            Тест функции поиска результата.
    
            Рассматривается случай: на вход подается ['Oct', 'Dod', 'Ico'].
            >>work(['Oct', 'Dod', 'Ico'], edge)
            40
            """
            edge = {"Tetrahedron": 4, "Hexahedron": 6, "Octahedron": 8, "Dodecahedron": 12, "Icosahedron": 20,
                    'Tet': 4, 'Hex': 6, "Oct": 8, "Dod": 12,
                    "Ico": 20}
            work_ = work(['Oct', 'Dod', 'Ico'], edge)
            self.assertEqual(work_, 40)
    
        def test_work_rib(self):
            """
            Тест функции поиска результата.
    
            Рассматривается случай: на вход подается ['Oct', 'Dod', 'Ico'].
            >>work(['Oct', 'Dod', 'Ico'], rib)
            72
            """
            rib = {"Tetrahedron": 6, "Hexahedron": 12, "Octahedron": 12, "Dodecahedron": 30, "Icosahedron": 30,
                   'Tet': 6, 'Hex': 12, "Oct": 12, "Dod": 30, "Ico": 30}
            work_ = work(['Oct', 'Dod', 'Ico'], rib)
            self.assertEqual(work_, 72)
    
        def test_work_angle(self):
            """
            Тест функции поиска результата.
    
            Рассматривается случай: на вход подается ['Oct', 'Dod', 'Ico'].
            >>work(['Oct', 'Dod', 'Ico'], angle)
            38
            """
            angle = {"Tetrahedron": 4, "Hexahedron": 8, "Octahedron": 6, "Dodecahedron": 20, "Icosahedron": 12,
                     'Tet': 4, 'Hex': 8, "Oct": 6, "Dod": 20, "Ico": 12}
            work_ = work(['Oct', 'Dod', 'Ico'], angle)
            self.assertEqual(work_, 38)
    
    
    def test(edge, arr_):
        """
        Функция проверки ввода.
    
        Функция проверяет входной аргумент на вхождение его в словарь Платоновых тел. Если входной аргумент
        не обнаружен в словаре, то прерывается с предупреждением.
    
        Args:
            :arg n: строка, которая вводится пользователем.
    
        Returns:
            :return arr_: список Платоновых тел.
    
        Example:
            >>test(['Oct', 'Dod', 'Ico'])
            ['Oct', 'Dod', 'Ico']
            >>test(['Oct'])
            ['Oct']
            >>test(['5'])
            "Введено неверное имя для Платонова тела!"
    
        """
        arr_ = [q for q in arr_.split()]
        for item in arr_:
            if item not in edge:
                print("Введено неверное имя для Платонова тела!")
                break
        return arr_
    
    
    def work(arr_, slovar):
        """
        Функция суммирования искомых велечин.
    
        Функция  находит соответсвующее значение соответствующего ключа и прибавляет его к счетчику.
    
        Args:
            :arg arr_: список Платоновых тел
            :arg slovar: словарь искомых велечин Платоновых тел.
    
        Returns:
            :return count: результат сложения
    
        Example:
            >>work(['Oct'],edge)
            8
            >>work(['Dod'],rib)
            30
    
        """
        count = 0
        try:
            for i in range(len(arr_)):
                count += slovar[arr_[i]]
        except KeyError:
            count = 'Error'
            return count
        else:
            return count
    
    
    def main():
        print(sys.argv)
        edge = {"Tetrahedron": 4, "Hexahedron": 6, "Octahedron": 8, "Dodecahedron": 12, "Icosahedron": 20,
                'Tet': 4, 'Hex': 6, "Oct": 8, "Dod": 12,
                "Ico": 20}
        rib = {"Tetrahedron": 6, "Hexahedron": 12, "Octahedron": 12, "Dodecahedron": 30, "Icosahedron": 30,
               'Tet': 6, 'Hex': 12, "Oct": 12, "Dod": 30, "Ico": 30}
        angle = {"Tetrahedron": 4, "Hexahedron": 8, "Octahedron": 6, "Dodecahedron": 20, "Icosahedron": 12,
                 'Tet': 4, 'Hex': 8, "Oct": 6, "Dod": 20, "Ico": 12}
        arr_ = test(edge, input('Введите Платоновы тела через пробел - '))
        count_edge = work(arr_, edge)
        count_rib = work(arr_, rib)
        count_angle = work(arr_, angle)
        try:
            int(count_edge)
            int(count_rib)
            int(count_angle)
        except ValueError:
            pass
        else:
            print(count_rib, count_edge, count_angle)
        if input('Do u want some tests?') == 'Yes':
            unittest.main()
    
    
    if __name__ == "__main__":
        sys.exit(main())


![](https://sun9-69.userapi.com/c204516/v204516820/9261/tY93FQozEBA.jpg)